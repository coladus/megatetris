import pygame as pg
from copy import deepcopy
from random import choice


W, H = 10, 20
TILE = 45
GAME_RES = W * TILE, H * TILE
FPS = 60
PAUSED = False

pg.init()
game_sc = pg.display.set_mode(GAME_RES)
clock = pg.time.Clock()

grid = [pg.Rect( x*TILE, y*TILE, TILE, TILE) for x in range(W) for y in range(H)]

figures_pos = [[(-1, 0), (-2, 0), (0, 0), (1, 0)],
               [(0, -1), (-1, -1), (-1, 0), (0, 0)],
               [(-1, 0), (-1, 1), (0, 0), (0, -1)],
               [(0, 0), (-1, 0), (0, 1), (-1, -1)],
               [(0, 0), (0, -1), (0, 1), (-1, -1)],
               [(0, 0), (0, -1), (0, 1), (1, -1)],
               [(0, 0), (0, -1), (0, 1), (-1, 0)]]

figures = [[pg.Rect(x + W // 2, y + 1, 1, 1) for x, y in fig_pos] for fig_pos in figures_pos]

figure_rect = pg.Rect(0, 0, TILE -2, TILE - 2)

field = [[0 for i in range(W)] for j in range(H)]

anim_count, anim_speed, anim_limit = 0, 60, 2000

figure = deepcopy(choice(figures))

def check_borders():
    if figure[i].x < 0 or figure[i].x > W - 1:
        return False
    elif figure[i].y > H - 1 or field[figure[i].y][figure[i].x]:
        return False
    return True

while True:
    motion, rotate = 0, False
    game_sc.fill(pg.Color('black'))

    #control
    for event in pg.event.get():
        if event.type == pg.QUIT:
            exit()
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_q:
                exit()
            elif event.key == pg.K_p:
                PAUSED = not PAUSED
        if not PAUSED:
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_LEFT:
                    motion = -1
                elif event.key == pg.K_RIGHT:
                    motion = 1
                elif event.key == pg.K_DOWN:
                    anim_limit = 100
                elif event.key == pg.K_UP:
                    rotate = True
                elif event.key == pg.K_q:
                    exit()

    #move X
    figure_old = deepcopy(figure)
    for i in range(4):
        figure[i].x += motion
        if not check_borders():
            figure = deepcopy(figure_old)
            break
    #move Y
    anim_count += anim_speed
    if anim_count > anim_limit and not PAUSED:
        anim_count = 0
        figure_old = deepcopy(figure)
        for i in range(4):
            figure[i].y += 1
            if not check_borders():
                for i in range(4):
                    field[figure_old[i].y][figure_old[i].x] = pg.Color('white')
                figure = deepcopy(choice(figures))
                anim_limit = 2000
                break

    #rotate
    rotate_center = figure[0]
    figure_old = deepcopy(figure)
    if rotate:
        for i in range(4):
            x = figure[i].y - rotate_center.y
            y = figure[i].x - rotate_center.x
            figure[i].x = rotate_center.x - x
            figure[i].y = rotate_center.y + y
            if not check_borders():
                figure = deepcopy(figure_old)
                break

    #check lines
    line = H - 1
    for row in range(H-1, -1, -1):
        count = 0
        for i in range(W):
            if field[row][i]:
                count += 1
            field[line][i] = field [row][i]
        if count < W:
            line -= 1

    #draw grid

    [pg.draw.rect(game_sc, (40, 40, 40), i_rect, 1) for i_rect in grid]

    #draw figure
    for i in range(4):
        figure_rect.x = figure[i].x * TILE
        figure_rect.y = figure[i].y * TILE
        pg.draw.rect(game_sc, pg.Color('white'), figure_rect)

    #draw field
    for y, raw in enumerate(field):
        for x, col in enumerate(raw):
            if col:
                figure_rect.x, figure_rect.y = x*TILE, y*TILE
                pg.draw.rect(game_sc, col, figure_rect)

    #game over
    for i in range(W):
        if field[0][i]:
            field = [[0 for i in range(W)] for j in range(H)]
            anim_count, anim_speed, anim_limit = 0, 60, 2000

    pg.display.flip()
    clock.tick(FPS)
